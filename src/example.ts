//========================== 1
function concat(a: string, b: string): string {
    return `${a} ${b}`;
}
let result = concat('Hello', 'World');
console.log(result); //Hello World


//========================== 2
interface ITask {
    howIDoIt: string;
    simeArray: [string, string, number];
    withData: [
        {
            howIDoIt: string;
            simeArray: [string, number];
        }
    ]
}

const myHometask: ITask = {
    howIDoIt: "I Do It Wel",
    simeArray: ["string one", "string two", 42],
    withData: [{ howIDoIt: "I Do It Wel", simeArray: ["string one", 23] }],
}


//========================== 3
interface MyArray<T> {
    [N: number]: T;
    reduce (callbackfn: (previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T): T;
}

const myArray: MyArray<number> = [1, 2, 3];
let resultReduce = myArray.reduce((a, b) => a + b);
console.log(resultReduce); //6
