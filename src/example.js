"use strict";
//========================== 1
function concat(a, b) {
    return a + " " + b;
}
var result = concat('Hello', 'World');
console.log(result); //Hello World
var myHometask = {
    howIDoIt: "I Do It Wel",
    simeArray: ["string one", "string two", 42],
    withData: [{ howIDoIt: "I Do It Wel", simeArray: ["string one", 23] }],
};
var myArray = [1, 2, 3];
var resultReduce = myArray.reduce(function (a, b) { return a + b; });
console.log(resultReduce); //6
